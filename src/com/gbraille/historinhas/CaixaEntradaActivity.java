/**
 * Copyright (C) 2014 Antonio Rodrigo
 * 
 * This file is part of GBraille Project.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; If not, see <http://www.gnu.org/licenses/>.
 */

package com.gbraille.historinhas;

import java.util.ArrayList;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.telephony.PhoneNumberUtils;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bugsense.trace.BugSenseHandler;

import de.akquinet.android.androlog.Log;

import com.gbraille.libraries.*;

public class CaixaEntradaActivity extends ListActivity implements OnInitListener  {
	private ArrayList<String> results = new ArrayList<String>();
	ListView listview;
	private final String TAG = "CaixaEntradaActivity";
	
	TextToSpeech myTTS;
	
	/* accelerometer vars */
	private SensorManager mSensorManager;
	private ShakeEventListener mSensorListener;
	private SensorEventListener sensorEventListener;
	
	/* accelerometer vars - using gSensor */
	private Sensor mAccelerometer;    
    private ShakeDetectorUsingGsensor mShakeDetectorUsingGSensor;
    
    private BugsenseFunctions bugsenseFunctions;
    private LogFunctions logFunctions;
    private DatabaseFunctions databaseFunctions;
    private TextToSpeechFunctions textToSpeechFunctions;
	
	@SuppressLint("InlinedApi")
	private void toggleFullScreen() {
		// desabilitar a status bar - android <= 4.0
		if (Build.VERSION.SDK_INT < 16) {
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
		// desabilitar a status bar - android >= 4.1
		else{
			int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
		               | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
		               | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
		               | View.SYSTEM_UI_FLAG_LOW_PROFILE
		               | View.SYSTEM_UI_FLAG_FULLSCREEN
		               | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
			getWindow().getDecorView().setSystemUiVisibility(mUIFlag);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		toggleFullScreen();
		
		databaseFunctions = new DatabaseFunctions();
		bugsenseFunctions = new BugsenseFunctions(this);
		
		logFunctions = new LogFunctions(true,"hist");
		if (logFunctions.getLogActivated()){
			Log.deactivateLogging();
		}
		
		BugSenseHandler.initAndStartSession(CaixaEntradaActivity.this, bugsenseFunctions.getBugsenseKeyApp());
		
		setContentView(R.layout.activity_caixa_entrada);
		
		textToSpeechFunctions = new TextToSpeechFunctions();
		
		//Add it to the list or do whatever you wish to
		listview = this.getListView();
		
		SQLiteDatabase myDB= null;
		
		try {
			myDB = this.openOrCreateDatabase(databaseFunctions.getNomeBD(), 0, null);
			myDB.beginTransaction();
			
			String selectQuery = "SELECT id, telefoneorigem, texto FROM mensagensrecebidas ORDER BY id desc";
			
			Cursor cursor = myDB.rawQuery(selectQuery, null);
			int count = cursor.getCount();

			int contador = 0;
            while(cursor.moveToNext()){
            	contador++;
            	String telefoneOrigem = cursor.getString(cursor.getColumnIndex("telefoneorigem"));
            	telefoneOrigem = PhoneNumberUtils.formatNumber(telefoneOrigem);
            	StringBuilder telefoneOrigemFormatado = formatPhoneNumber(telefoneOrigem);
            	String msg = cursor.getString(cursor.getColumnIndex("texto"));
            	String txt = "Mensagem " + contador + " de " + count + ". Enviado por: " + telefoneOrigemFormatado + " .  Mensagem:" + msg;
            	results.add( txt );
            }
            
            if (count == 0){
            	String txt = "N�O H� MENSAGENS EM SUA CAIXA DE ENTRADA";
            	results.add( txt );
            	speakWords(txt);
            }
            
            myDB.setTransactionSuccessful(); 
    		myDB.endTransaction(); 
    		myDB.close();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),
                      android.R.layout.simple_list_item_1, android.R.id.text1,results);

            listview.setAdapter(adapter);
            
            listview.setOnItemClickListener(new OnItemClickListener() {                
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
					String item = ((TextView)arg1).getText().toString();					
					// TODO Auto-generated method stub
					speakWords( item );
				}
            });
		}
		catch(Exception e) {
			Log.e("Error", "Error", e);
		}
		
		Intent checkIntent = new Intent();
		checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkIntent, textToSpeechFunctions.getReqTTSStatusCheck());
		
		// shake implementation
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensorEventListener = mSensorListener;
		
	    mSensorListener = new ShakeEventListener();

	    mSensorListener.setOnShakeListener(new ShakeEventListener.OnShakeListener() {
	      public void onShake() {
	    	Log.i(TAG,LogMessages.MSG_SHAKE);
          	if (myTTS != null) {
      			myTTS.stop();
      		}
      		BugSenseHandler.closeSession(getActivity());
      		mSensorManager.unregisterListener(sensorEventListener);
      		Intent intent = new Intent();
      		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      		finish();
	      }
	    });
	    
	    /* ShakeDetector initialization */
	    /*
	    mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
	    sensorEventListener = mShakeDetectorUsingGSensor;
	    
	    mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);        
        
        mShakeDetectorUsingGSensor = new ShakeDetectorUsingGsensor();
        mShakeDetectorUsingGSensor.setOnShakeListener(new OnShakeListener() { 
            @Override
            public void onShake(int count) {
            	Log.i(TAG,"Shake!");
            	if (myTTS != null) {
        			myTTS.stop();
        		}
        		BugSenseHandler.closeSession(getActivity());            	
            	mSensorManager.unregisterListener(mShakeDetectorUsingGSensor);
            	
        		Intent intent = new Intent();
        		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        		finish();
            }
        });   
		*/
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == textToSpeechFunctions.getReqTTSStatusCheck()) {
			switch(resultCode){
				case TextToSpeech.Engine.CHECK_VOICE_DATA_PASS:
					Log.i(TAG, LogMessages.MSG_TTS_UP_RUNNING);
					myTTS = new TextToSpeech(getApplicationContext(), this);
					break;
				case TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL:					
					Intent installTTSIntent = new Intent();
					installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
					startActivity(installTTSIntent);
					break;
				default:
					Log.i(TAG,LogMessages.MSG_TTS_NOT_AVAILABLE);
			}
	    }
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void onInit(int arg0) {
		// TODO Auto-generated method stub
		if (arg0 == TextToSpeech.SUCCESS) {
		    myTTS.setLanguage(Locale.getDefault());
		}
		else {
			myTTS = null;
			Log.e(TAG, LogMessages.MSG_TTS_FAILED);			
		}
	}
	
	@Override
	protected void onResume() {
		BugSenseHandler.startSession(CaixaEntradaActivity.this);
		super.onResume();
		logFunctions.logTextFile(LogMessages.MSG_MESSAGE_BOX_ACCESSED);
		//mSensorManager.registerListener(mShakeDetectorUsingGsensor, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
		mSensorManager.registerListener(mSensorListener,
		        mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
		        SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	protected void onPause() {
		BugSenseHandler.closeSession(CaixaEntradaActivity.this);
		//mSensorManager.unregisterListener(mShakeDetectorUsingGsensor);
		mSensorManager.unregisterListener(mSensorListener);
		super.onPause();
		if (myTTS != null){
        	myTTS.stop();
        }
	}
	
	@Override
    protected void onDestroy() {
        super.onDestroy();
        if (myTTS != null){
        	myTTS.shutdown();
        }
        logFunctions.logTextFile(LogMessages.MSG_MESSAGE_BOX_CLOSED);
    }
	
	protected void speakWords(String speech) {
        //speak straight away
		if (myTTS != null){
			myTTS.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
		}
    }
	
	/* ---------------------- LOCAL METHODS ---------------------- */
	/**
	 * Convert the phone number to the format (xx) 12 34 56 78
	 *
	 * @author      Antonio Rodrigo
	 * @version		1.0
	 * @return		formatted phone
	 */
    protected StringBuilder formatPhoneNumber(String string) {
        if (string.startsWith("0")) {
            string = string.replace("0", "");
        }
        StringBuilder sb = new StringBuilder();  
        
        sb.append("{")
        .append("(")
        .append(string.substring(0, 2)).append(") ")
        .append(string.substring(2, 4)).append(" ")
        .append(string.substring(4, 6)).append(" ")
        .append(string.substring(6, 8)).append(" ")
        .append(string.substring(8, string.length()))
        .append("}");
        
        return sb;
    }
    
    /**
	 * retrieves the current activity
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 * @return current activity
	 */
	protected Activity getActivity() {
		return CaixaEntradaActivity.this;
	}
}

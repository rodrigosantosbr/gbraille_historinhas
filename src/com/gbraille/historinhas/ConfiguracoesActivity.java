/**
 * Copyright (C) 2014 Antonio Rodrigo
 * 
 * This file is part of GBraille Project.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; If not, see <http://www.gnu.org/licenses/>.
 */

package com.gbraille.historinhas;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import de.akquinet.android.androlog.Log;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.gbraille.libraries.*;

public class ConfiguracoesActivity extends AccessibleAbstractActivityBrailleTemplate {
	/* Google Cloud Messaging variables */
	private GoogleCloudMessaging gcm; 
    private String regid;
    
    String field;
    
    private CloudMessagingFunctions cloudMessagingFunctions;
    private DatabaseFunctions databaseFunctions;
    private LogFunctions logFunctions;
    private ViberFunctions viberFunctions;
        
	/**
	 * onclick actions for buttons
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0 
	 */
	public void setOnClickForButtons() {
		/* ajuda - onclick event */
		buttons[0].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String msg = "AJUDA";
				speakWords(myTTS, msg);
			}
		});
		
		/* teacher phone number - onclick event */
		buttons[1].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				stopServices();
				field = "telefoneprofessor";
				logFunctions.logTextFile(LogMessages.MSG_BUTTON_TEACHER_PHONE_NUMBER_INSERT_TOUCHED);
				callBrailleKeyboard(getActivity());
			}
		});

		/* phone sync with google cloud messaging - onclick event */
		buttons[2].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {				
		        if (databaseFunctions.createDatabase(context) == false){
		        	Log.i(TAG, LogMessages.MSG_BD_ERROR_CREATION);
		        }
		        else{
		        	// Check device for Play Services APK. If check succeeds, proceed with GCM registration.
		        	if (checkPlayServices()) {
		        		gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
		        		regid = getRegistrationId();
		        		Log.i(TAG, "regid = " + regid);		        		
		        		if (regid.equals("") || regid.isEmpty() || TextUtils.isEmpty(regid)) {
		        			Log.i(TAG, LogMessages.MSG_NEEDS_GCM_SERVICE_REGISTRATION);
		        			preparingInstallationInBackground();
		        		}
		        		else {
		        			Log.i(TAG, LogMessages.MSG_ALREADY_REGISTERED_GCM_SERVICE);
		        			speakWords(myTTS, getString(R.string.speakDeviceIsRegistered));
		        		}
		        	}
		        	else {
		        		Log.i(TAG, LogMessages.MSG_NO_VALID_GOOGLE_PLAY_SERVICES_VALID_APK);
		        		speakWords(myTTS, getString(R.string.speakDeviceRegistrationFailed));
		        	}
		        }
			}
		});

		/* main menu - onclick event */
		buttons[3].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				returnToMainMenu();
			}
		});
	}

	/**
	 * Called when the activity is first created.
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		screenName = "Historinhas: Op��es";
		super.onCreate(savedInstanceState);	
		databaseFunctions = new DatabaseFunctions();
		logFunctions = new LogFunctions(true,"hist");
		cloudMessagingFunctions = new CloudMessagingFunctions(this);
		viberFunctions = new ViberFunctions();
		
		nodeDescriptionString = new String[totalButtons];
		
		fillScreenOptionsFromXMLFile("configuracoesoptions.xml","option");
	}

	/**
	 * retrieves the Layout resource
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 * @return layout resource id
	 */
	@Override
	protected int getLayoutResourceId() {
		return R.layout.activity_configuracoes;
	}

	/**
	 * retrieves the current activity
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 * @return current activity
	 */
	@Override
	protected Activity getActivity() {
		return ConfiguracoesActivity.this;
	}

	/**
	 * adds the buttons to variables
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 */
	@Override
	protected void setViews() {
		buttons = new ImageButton[totalButtons];		
		buttons[0] = (ImageButton) findViewById(R.id.botao1);
		buttons[1] = (ImageButton) findViewById(R.id.botao2);
		buttons[2] = (ImageButton) findViewById(R.id.botao3);
		buttons[3] = (ImageButton) findViewById(R.id.botao4);				
	}

	/**
	 * set the number of buttons in the current activity
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0 
	 */
	@Override
	protected void setNumberButtons() {
		totalButtons = 4;
	}
	
	/**
	 * retrieves the Activity's Linear Layout
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 * @return LinearLayout
	 */
	@Override
	protected LinearLayout getLayout() {
		return (LinearLayout) this.findViewById(R.id.layoutMenuOpcoes);
	}
	
	/* -------------------- GOOGLE CLOUD MESSAGING AREA -------------------- */

	private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, cloudMessagingFunctions.getPlayServicesResolutionRequest()).show();
            } 
            else {
                Log.i(TAG, LogMessages.MSG_THIS_DEVICE_NOT_SUPPORTED);
                finish();
            }
            return false;
        }
        return true;
    }
	
    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGcmPreferences() {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.        
        return getSharedPreferences(ConfiguracoesActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }
    
    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * If result is empty, the app needs to register.
     * @author      Antonio Rodrigo
	 * @version		1.0
     * @return      registration ID or empty string if there is no existing registration ID.
     */
    private String getRegistrationId() {
        final SharedPreferences prefs = getGcmPreferences();
        String registrationId = prefs.getString(cloudMessagingFunctions.getPropertyRegId(), "");
        if (registrationId.equals("") || registrationId.isEmpty() || TextUtils.isEmpty(registrationId)) {
            Log.i(TAG, LogMessages.MSG_GCM_REGISTRATION_NOT_FOUND);
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(cloudMessagingFunctions.getPropertyAppVersion(), Integer.MIN_VALUE);
        int currentVersion = getAppVersion();
        if (registeredVersion != currentVersion) {
            Log.i(TAG, LogMessages.MSG_APP_VERSION_CHANGED);
            return "";
        }
        return registrationId;
    }
	
    /**
	 * Retrieves the Application version Code
     * @author      Antonio Rodrigo
	 * @version		1.0 
     * @return 		Application's version code
     */
    private int getAppVersion() {
        try {
            PackageInfo packageInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
            return packageInfo.versionCode;
        } 
        catch (NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    
    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void preparingInstallationInBackground() {
        new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(cloudMessagingFunctions.getSenderId());
                    msg = LogMessages.MSG_GCM_DEVICE_REGISTERED + regid;
 
                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    // sendRegistrationIdToBackend();
 
                    // For this demo: we don't need to send it because the device will send
                    // upstream messages to a server that echo back the message using the
                    // 'from' address in the message.
 
                    // Persist the regID - no need to register again.
                    storeRegistrationId(getApplicationContext(), regid);
                    
                    sendRegistrationIdToBackend();                    
                } 
                catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                Log.i(TAG, msg);
                return msg;
            }
 
            @Override
            protected void onPostExecute(String msg) {
                // DO NOTHING
            }
            
        }.execute(null, null, null);
    }
    
    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {
		String msg;
    	// fazer um cadastro no webservice
    	if (!regid.equals("") && !TextUtils.isEmpty(regid)){
    		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
    		params.add(new BasicNameValuePair("numerotel", viberFunctions.getViberPhoneNumber(context)));
    		params.add(new BasicNameValuePair("identificador", regid));    		
        
    		DefaultHttpClient httpclient = new DefaultHttpClient();            
    		HttpPost httppost = new HttpPost(cloudMessagingFunctions.getServerUrl());        
    		Log.i(TAG, "SERVER URL = " + cloudMessagingFunctions.getServerUrl());
    		try {
    			httppost.setEntity(new UrlEncodedFormEntity(params));
    			HttpResponse response = httpclient.execute(httppost);
    			int responseCode = response.getStatusLine().getStatusCode();
    			if(responseCode == 200) {
    				Log.i(TAG, LogMessages.MSG_GCM_PHONE_SYNC_SUCCESSFULLY);
    				speakWords(myTTS, getString(R.string.speakInstallationSuccess));
    			}
    			else {
    				Log.i(TAG, LogMessages.MSG_GCM_PHONE_SYNC_ERROR);
    				speakWords(myTTS, getString(R.string.speakDeviceRegistrationFailed));
                }
    		}
    		catch (Exception e) {
    			Log.i(TAG, LogMessages.MSG_GCM_PHONE_SYNC_ERROR);
                speakWords(myTTS, getString(R.string.speakDeviceRegistrationFailed));
    		}
    	}
    }
    
	/**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences();
        int appVersion = getAppVersion();
        Log.i(TAG, LogMessages.MSG_SAVING_REGID + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(cloudMessagingFunctions.getPropertyRegId(), regId);
        editor.putInt(cloudMessagingFunctions.getPropertyAppVersion(), appVersion);
        editor.commit();
    }  
    
    @SuppressWarnings("deprecation")
	public void setTts() { 
        if( Build.VERSION.SDK_INT  >= 15 ){
            myTTS.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onDone(String utteranceId){
                	Log.i(TAG, "Text to speech finished");
                	if (utteranceId.equals("EXIT_APPLICATION")){
			        	hapticFunctions.vibrateOnExit();
			        	destroyServices();
			        	finish();
					}
                	else if (utteranceId.equals("STARTING_APPLICATION")){
            			ConfiguracoesActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            	// vibrates and ebables keyboard when application starts
                            	hapticFunctions.vibrate(1);                    			
                            }
                        });      			
                	}
                }
 
                @Override
                public void onError(String utteranceId){
                	Log.i(TAG, "Error in processing Text to speech");
                }
 
                @Override
                public void onStart(String utteranceId){
                	Log.i(TAG, "Started speaking");
                }
            });
        }
    }
    
    /**
	 * fillScreenOptionsFromXMLFile
	 *     Loads the contents of the file named 'applications.xml' and get its params
	 * @author Antonio Rodrigo
	 * @version 1.0
	 */
	private void fillScreenOptionsFromXMLFile(String fileName, String nodeScreenOptionsTag) {
		try {
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();    
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder(); 
		    Document doc = dBuilder.parse(getAssets().open(fileName));
		    doc.getDocumentElement().normalize();

		    NodeList nodeScreenOptions = doc.getElementsByTagName(nodeScreenOptionsTag);
		    
		    for (int i = 0; i < nodeScreenOptions.getLength(); i++) {
		    	final int j = i;
		    	Node item = nodeScreenOptions.item(i);

		    	if (item.getNodeType() == Node.ELEMENT_NODE) {
		    		Element element = (Element) item;
		    		Node nodeDescricao = element.getElementsByTagName("description").item(0).getChildNodes().item(0);
		    		Node nodeIcone = element.getElementsByTagName("icon").item(0).getChildNodes().item(0);
		    		
		    		Log.i(TAG, nodeDescricao.getNodeValue());
		    		
		    		final String desc = nodeDescricao.getNodeValue();
		    		
		    		nodeDescriptionString[i] = nodeDescricao.getNodeValue();
		    		
		    		int imageResource = getResources().getIdentifier(nodeIcone.getNodeValue(), null, getPackageName());
		    		Drawable res = getResources().getDrawable(imageResource);
		    				    
		    		buttons[i].setTag(nodeDescricao.getNodeValue());
		    		buttons[i].setImageDrawable(res);
		    		buttons[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
						public void onFocusChange(View v, boolean hasFocus) {
							if (hasFocus) {
								Log.i(TAG, "Button " + desc + " was selected");
								logFunctions.logTextFile("Button " + desc + " was selected");
								if (j == 0 || j == 1 || j == 2){
									speakButton(desc, 0, 1.0f);
								}
								else{
									speakButton(desc, 1.0f, 0);
								}
							}
						}
					});
		    		
		    	}
		    }
		}
		catch(Exception e){
		     e.printStackTrace();
		}
	}
	
	
	/** 
	 * This is the callback from the TTS engine check
	 * if a TTS is installed we create a new TTS instance (which in turn calls onInit)
	 * if not then we will create an intent to go off and install a TTS engine
	 * 
	 * Metodo que analisa o retorno dado pelo teclado LeBraille 
	 * 
	 * @author    Antonio Rodrigo
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */  
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		String texto = "";
		Log.i(TAG, LogMessages.MSG_ON_ACTIVITY_RESULT);		
		Log.i(TAG, LogMessages.MSG_REQUEST_CODE + requestCode);
		if (requestCode == textToSpeechFunctions.getReqTTSStatusCheck()) {
			switch(resultCode){
				case TextToSpeech.Engine.CHECK_VOICE_DATA_PASS:
					Log.i(TAG, LogMessages.MSG_TTS_UP_RUNNING);
					myTTS = new TextToSpeech(getApplicationContext(), this);
					break;
				case TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL:
					Intent installTTSIntent = new Intent();
					installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
					startActivity(installTTSIntent);
					break;				
				default:
					Log.i(TAG, LogMessages.MSG_TTS_NOT_AVAILABLE);
			}
	    }
		else if (requestCode == keyboardFunctions.getKeyboardResponse()){
	        if(resultCode == RESULT_OK){
	        	if(data.getExtras().containsKey("key")){
	        		texto = data.getStringExtra("key").trim();
	        	}
	        	Log.i(TAG,LogMessages.MSG_KEYBOARD_RETURNED_TEXT + texto);
	           	logFunctions.logTextFile(LogMessages.MSG_KEYBOARD_RETURNED_TEXT + texto);	           	
	        }
	        
	        // if nothing was inserted at keyboard
	        if (texto.equals("") || TextUtils.isEmpty(texto) || texto.length() == 0){
	        	speakWords(myTTS, getString(R.string.speakNothingWasInsertedFromKeyboard));
	        	return;
	        }
	        
	        if ( field.equals("telefoneprofessor") ){
		    	try{
		    		SQLiteDatabase db = this.openOrCreateDatabase(databaseFunctions.getNomeBD(), MODE_PRIVATE, null);
		    		db.beginTransaction();
		    		String table = "configuracoes";
	    	
		    		ContentValues newValues = new ContentValues();
		    		newValues.put("configuracao", "telefoneprofessor");
		    		newValues.put("valor", texto);
	        			    		    
		    		if (databaseFunctions.isTableExists(db, table)){		    			
		    			db.update(table, newValues, "id=1", null);
	        	   		db.setTransactionSuccessful();	        	   				    			
	    		
		    			Log.i(TAG, LogMessages.MSG_BD_TEXT_MESSAGE_SAVED);
		    			logFunctions.logTextFile("TELEFONE DO PROFESSOR CADASTRADO COM SUCESSO: " + texto);
		    			speakWordsWithDelay(myTTS, getString(R.string.speakTextWasSaved) + " " + texto, 1000);
		    			
	        	   		Log.i(TAG, LogMessages.MSG_BD_TEACHER_PHONE_NUMBER_SAVED);
		    		}
		    		else{
		    			Log.i(TAG,LogMessages.MSG_BD_ERROR_TABLE_CONFIGURACOES_DOES_NOT_EXISTS);
			    		logFunctions.logTextFile(LogMessages.MSG_BD_ERROR_TABLE_CONFIGURACOES_DOES_NOT_EXISTS);
		    			speakWordsWithDelay(myTTS, getString(R.string.speakErrorSaveText), 1000);
		    		}
		    		
		    		db.endTransaction();
	    			db.close();
		    	}
		    	catch ( Exception e ) {
		    		Log.i(TAG,LogMessages.MSG_BD_ERROR_FAIL_SAVE_TEXT);
		    		logFunctions.logTextFile(LogMessages.MSG_BD_ERROR_FAIL_SAVE_TEXT);
		    		speakWordsWithDelay(myTTS, getString(R.string.speakErrorSaveText), 1000);
		        }
		    }
		    
		}
	}
}
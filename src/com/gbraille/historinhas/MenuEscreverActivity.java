/**
 * Copyright (C) 2014 Antonio Rodrigo
 * 
 * This file is part of GBraille Project.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; If not, see <http://www.gnu.org/licenses/>.
 */

package com.gbraille.historinhas;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.text.TextUtils;
import de.akquinet.android.androlog.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.gbraille.libraries.*;

public class MenuEscreverActivity extends AccessibleAbstractActivityBrailleTemplate {
	/* represents the selected option (texto or telefonedestino) */
	private String field;
	
	/* database functions */
	private DatabaseFunctions databaseFunctions = new DatabaseFunctions();
	
	/**
	 * onclick actions for buttons
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0 
	 */
	protected void setOnClickForButtons() {		
		/* write message - onclick event */
		this.buttons[0].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				stopServices();
				field = "texto";
				logFunctions.logTextFile(LogMessages.MSG_BUTTON_WAS_ACCESSED + (String) buttons[0].getTag());
				callBrailleKeyboard(getActivity());
			}
		});

		/* insert phone number - onclick event */
		this.buttons[1].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				stopServices();
				field = "telefonedestino";
				logFunctions.logTextFile(LogMessages.MSG_BUTTON_WAS_ACCESSED + (String) buttons[1].getTag());
				callBrailleKeyboard(getActivity());
			}
		});

		/* send message onclick event */
		this.buttons[2].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				logFunctions.logTextFile(LogMessages.MSG_BUTTON_WAS_ACCESSED + (String) buttons[2].getTag());
				
				String destinationPhoneNumber, teacherphone, text;
				destinationPhoneNumber = teacherphone = text = "";			
				
				try{
					SQLiteDatabase myDB = openOrCreateDatabase(databaseFunctions.getNomeBD(), MODE_PRIVATE, null);					
					myDB.beginTransaction();
					
					if (databaseFunctions.isTableExists(myDB, "mensagem") == false || databaseFunctions.isTableExists(myDB, "configuracoes") == false){
						Log.i(TAG, LogMessages.MSG_BD_ERROR_MISSING_TABLES);
						speakWords(myTTS, getString(R.string.speakDatabaseError));
					}
					else{
						/* get phone number an the text to send */
						String selectQuery = "SELECT telefonedestino, texto FROM mensagem WHERE id = 1";
						Cursor cursor = myDB.rawQuery(selectQuery, null);
						cursor.moveToFirst();
						destinationPhoneNumber = cursor.getString(cursor.getColumnIndex("telefonedestino")).trim();
						text = cursor.getString(cursor.getColumnIndex("texto")).trim();
						cursor.close();
						
						/* get the teacher phone number */
						selectQuery = "SELECT configuracao, valor FROM configuracoes WHERE configuracao = 'telefoneprofessor' AND id = 1";
						cursor = myDB.rawQuery(selectQuery, null);
						cursor.moveToFirst();
						teacherphone = cursor.getString(cursor.getColumnIndex("valor")).trim();
						cursor.close();
						
						if (destinationPhoneNumber.equals("") || TextUtils.isEmpty(destinationPhoneNumber) || destinationPhoneNumber.length() == 0){
							speakWords(myTTS, getString(R.string.speakErrorPhoneNumberDestinationDoesNotExists));
						}
						else if (text.equals("") || TextUtils.isEmpty(text) || text.length() == 0){
							speakWords(myTTS, getString(R.string.speakErrorTextDoesNotExists));
						}
						else if (teacherphone.equals("") || TextUtils.isEmpty(teacherphone) || teacherphone.length() == 0){
							speakWords(myTTS, getString(R.string.speakErrorTeacherPhoneNumberDoesNotExists));
						}
						else{
							/* send the message */
							try {
								SmsManager smsManager = SmsManager.getDefault();
								smsManager.sendTextMessage(destinationPhoneNumber, null, text, null, null);
								Log.i(TAG, LogMessages.MSG_SMS_SENT + destinationPhoneNumber + " | texto : " + text);
								speakWords(myTTS, getString(R.string.speakSMSSentToDestination));
							}
							catch (Exception e) {
								Log.i(TAG, LogMessages.MSG_SMS_ERROR_SEND_SMS_TO_DESTINATION);
								speakWords(myTTS, getString(R.string.speakErrorSendSMSToDestination));
							}
							
							if (! teacherphone.equals(destinationPhoneNumber)){
								try {
									SmsManager smsManager = SmsManager.getDefault();
									smsManager.sendTextMessage(teacherphone, null, text, null, null);
									Log.i(TAG, LogMessages.MSG_TEACHER_PHONE + teacherphone + " | " + LogMessages.MSG_TEXT + text);
									Log.i(TAG, LogMessages.MSG_SMS_SENT);
								}
								catch (Exception e) {
									Log.i(TAG, LogMessages.MSG_SMS_ERROR_TO_SEND_TEACHER);
									// speakWords(myTTS, getString(R.string.speakSMSErrorToSendMessageTeacher));							
								}
							}
						}						
					}
					myDB.setTransactionSuccessful(); 
					myDB.endTransaction(); 
					myDB.close();
				}
				catch (Exception e) {
					Log.i(TAG, LogMessages.MSG_BD_ERROR_FAIL_GET_PHONE_NUMBERS_AND_TEXT);
					speakWords(myTTS, getString(R.string.speakErrorFailGetPhoneNumbersAndText));
					return;
				}				
			}
		});

		/* main menu - onclick event */
		this.buttons[3].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				returnToMainMenu();
			}
		});
	}

	/**
	 * Called when the activity is first created.
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.screenName = "Historinhas: Menu Escrever";
		super.onCreate(savedInstanceState);		
		nodeDescriptionString = new String[totalButtons];
		
		fillScreenOptionsFromXMLFile("menuescreveroptions.xml","option");
	}
	
	/** 
	 * This is the callback from the TTS engine check
	 * if a TTS is installed we create a new TTS instance (which in turn calls onInit)
	 * if not then we will create an intent to go off and install a TTS engine
	 * 
	 * Metodo que analisa o retorno dado pelo teclado LeBraille 
	 * 
	 * @author    Antonio Rodrigo
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */  
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		String texto = "";
		Log.i(TAG, LogMessages.MSG_ON_ACTIVITY_RESULT);		
		Log.i(TAG, LogMessages.MSG_REQUEST_CODE + requestCode);
		if (requestCode == textToSpeechFunctions.getReqTTSStatusCheck()) {
			switch(resultCode){
				case TextToSpeech.Engine.CHECK_VOICE_DATA_PASS:
					Log.i(TAG, LogMessages.MSG_TTS_UP_RUNNING);
					myTTS = new TextToSpeech(getApplicationContext(), this);
					break;
				case TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL:
					Intent installTTSIntent = new Intent();
					installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
					startActivity(installTTSIntent);
					break;				
				default:
					Log.i(TAG, LogMessages.MSG_TTS_NOT_AVAILABLE);
			}
	    }
		else if (requestCode == keyboardFunctions.getKeyboardResponse()){
	        if(resultCode == RESULT_OK){
	        	if(data.getExtras().containsKey("key")){
	        		texto = data.getStringExtra("key").trim();
	        	}
	        	Log.i(TAG,LogMessages.MSG_KEYBOARD_RETURNED_TEXT + texto);
	           	logFunctions.logTextFile(LogMessages.MSG_KEYBOARD_RETURNED_TEXT + texto);	           	
	        }
	        
	        // if nothing was inserted at keyboard
	        if (texto.equals("") || TextUtils.isEmpty(texto) || texto.length() == 0){
	        	speakWords(myTTS, getString(R.string.speakNothingWasInsertedFromKeyboard));
	        	return;
	        }
	        
	        if ( field.equals("texto") ){
		    	try{
		    		SQLiteDatabase db = this.openOrCreateDatabase(databaseFunctions.getNomeBD(), MODE_PRIVATE, null);
		    		db.beginTransaction();
		    		String table = "mensagem";
	    	
		    		ContentValues newValues = new ContentValues();
		    		newValues.put("texto", texto);
	    		    
		    		if (databaseFunctions.isTableExists(db, table)){
		    			db.update(table, newValues, "id=1", null);	    	
		    			db.setTransactionSuccessful();		    			
	    		
		    			Log.i(TAG, LogMessages.MSG_BD_TEXT_MESSAGE_SAVED);
		    			logFunctions.logTextFile("MENSAGEM CADASTRADA: " + texto);
		    			speakWordsWithDelay(myTTS, getString(R.string.speakTextWasSaved) + " " + texto, 1000);
		    		}
		    		else{
		    			Log.i(TAG,LogMessages.MSG_BD_ERROR_TABLE_MENSAGEM_DOES_NOT_EXISTS);
			    		logFunctions.logTextFile(LogMessages.MSG_BD_ERROR_TABLE_MENSAGEM_DOES_NOT_EXISTS);
		    			speakWordsWithDelay(myTTS, getString(R.string.speakErrorSaveText), 1000);
		    		}
		    		
		    		db.endTransaction();
	    			db.close();
		    	}
		    	catch ( Exception e ) {
		    		Log.i(TAG,LogMessages.MSG_BD_ERROR_FAIL_SAVE_TEXT);
		    		logFunctions.logTextFile(LogMessages.MSG_BD_ERROR_FAIL_SAVE_TEXT);
		    		speakWordsWithDelay(myTTS, getString(R.string.speakErrorSaveText), 1000);
		        }
		    }
		    else if ( field.equals("telefonedestino") ){
		    	if( PhoneNumberUtils.isWellFormedSmsAddress(texto) == false){
		    		Log.i(TAG,LogMessages.MSG_BD_ERROR_DESTINATION_PHONE_NUMBER_NOT_UPDATED);
		    		speakWordsWithDelay(myTTS, getString(R.string.speakInvalidPhoneNumber), 1000);
		    	}
		    	else{
		    		try{
		    			SQLiteDatabase db = this.openOrCreateDatabase(databaseFunctions.getNomeBD(), MODE_PRIVATE, null);
		    			db.beginTransaction();
		    			String table = "mensagem";
	    	
		    			ContentValues newValues = new ContentValues();
		    			newValues.put("telefonedestino", texto);
	    		
		    			if (databaseFunctions.isTableExists(db, table)){
		    				db.update(table, newValues, "id=1", null);
		    				db.setTransactionSuccessful();		    				
		    				Log.i(TAG,LogMessages.MSG_BD_DESTINATION_PHONE_NUMBER_SAVED);
		    				logFunctions.logTextFile(LogMessages.MSG_PHONE_NUMBER_SAVED + texto);
		    				speakWordsWithDelay(myTTS, getString(R.string.speakPhoneDestinationWasSaved) + " " + texto, 1000);
		    			}
		    			else{
		    				Log.i(TAG,LogMessages.MSG_BD_ERROR_TABLE_MENSAGEM_DOES_NOT_EXISTS);
				    		logFunctions.logTextFile(LogMessages.MSG_BD_ERROR_TABLE_MENSAGEM_DOES_NOT_EXISTS);
				    		speakWordsWithDelay(myTTS, getString(R.string.speakErrorSavePhoneNumberDestination), 1000);
		    			}
		    			
		    			db.endTransaction();
	    				db.close();    		
		    		}
			    	catch ( Exception e ) {
			    		Log.i(TAG,LogMessages.MSG_BD_ERROR_FAIL_SAVE_DESTINATION_PHONE_NUMBER);
			    		logFunctions.logTextFile(LogMessages.MSG_BD_ERROR_FAIL_SAVE_DESTINATION_PHONE_NUMBER);
			    		speakWordsWithDelay(myTTS, getString(R.string.speakErrorSavePhoneNumberDestination), 1000);
			        }
		    	}
		    }
		}
	}
	
	/**
	 * retrieves the Layout resource
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 * @return layout resource id
	 */
	@Override
	protected int getLayoutResourceId() {
		return R.layout.activity_menu_escrever;
	}
	
	/**
	 * retrieves the current activity
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 * @return current activity
	 */
	@Override
	protected Activity getActivity() {
		return MenuEscreverActivity.this;
	}
	
	/**
	 * adds the buttons to variables
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 */
	@Override
	protected void setViews() {
		this.buttons = new ImageButton[totalButtons];		
		this.buttons[0] = (ImageButton) findViewById(R.id.botao1);
		this.buttons[1] = (ImageButton) findViewById(R.id.botao2);
		this.buttons[2] = (ImageButton) findViewById(R.id.botao3);
		this.buttons[3] = (ImageButton) findViewById(R.id.botao4);
	}

	/**
	 * set the number of buttons in the current activity
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0 
	 */
	@Override
	protected void setNumberButtons() {
		this.totalButtons = 4;
	}
	
	/**
	 * retrieves the Activity's Linear Layout
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 * @return LinearLayout
	 */
	@Override
	protected LinearLayout getLayout() {
		return (LinearLayout) this.findViewById(R.id.layoutMenuEscrever);
	}
	
	protected void setTts() { 
        if( Build.VERSION.SDK_INT  >= 15 ){
        	final String thisTAG = this.TAG;
            myTTS.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onDone(String utteranceId){
                	Log.i(thisTAG, "Text to speech finished");
                	if (utteranceId.equals("EXIT_APPLICATION")){
			        	hapticFunctions.vibrateOnExit();
			        	destroyServices();
			        	finish();
					}
                	else if (utteranceId.equals("STARTING_APPLICATION")){
            			MenuEscreverActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            	// vibrates and ebables keyboard when application starts
                            	hapticFunctions.vibrate(1);
                            }
                        });      			
                	}
                }
 
                @Override
                public void onError(String utteranceId){
                	Log.i(thisTAG, "Error in processing Text to speech");
                }
 
                @Override
                public void onStart(String utteranceId){
                	Log.i(thisTAG, "Started speaking");
                }
            });
        }
    }
	
	/**
	 * fillScreenOptionsFromXMLFile
	 *     Loads the contents of the file named 'applications.xml' and get its params
	 * @author Antonio Rodrigo
	 * @version 1.0
	 */
	private void fillScreenOptionsFromXMLFile(String fileName, String nodeScreenOptionsTag) {
		try {
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();    
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder(); 
		    Document doc = dBuilder.parse(getAssets().open(fileName));
		    doc.getDocumentElement().normalize();

		    NodeList nodeScreenOptions = doc.getElementsByTagName(nodeScreenOptionsTag);
		    
		    for (int i = 0; i < nodeScreenOptions.getLength(); i++) {
		    	final int j = i;
		    	Node item = nodeScreenOptions.item(i);

		    	if (item.getNodeType() == Node.ELEMENT_NODE) {
		    		Element element = (Element) item;
		    		Node nodeDescricao = element.getElementsByTagName("description").item(0).getChildNodes().item(0);
		    		Node nodeIcone = element.getElementsByTagName("icon").item(0).getChildNodes().item(0);
		    		
		    		Log.i(TAG, nodeDescricao.getNodeValue());
		    		
		    		final String desc = nodeDescricao.getNodeValue();
		    		
		    		nodeDescriptionString[i] = nodeDescricao.getNodeValue();
		    		
		    		int imageResource = getResources().getIdentifier(nodeIcone.getNodeValue(), null, getPackageName());
		    		Drawable res = getResources().getDrawable(imageResource);
		    				    
		    		buttons[i].setTag(nodeDescricao.getNodeValue());
		    		buttons[i].setImageDrawable(res);
		    		buttons[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
						public void onFocusChange(View v, boolean hasFocus) {
							if (hasFocus) {
								Log.i(TAG, "Button " + desc + " was selected");
								logFunctions.logTextFile("Button " + desc + " was selected");
								if (j == 0 || j == 1 || j == 2){
									speakButton(desc, 0, 1.0f);
								}
								else{
									speakButton(desc, 1.0f, 0);
								}
							}
						}
					});
		    		
		    	}
		    }
		}
		catch(Exception e){
		     e.printStackTrace();
		}
	}
}
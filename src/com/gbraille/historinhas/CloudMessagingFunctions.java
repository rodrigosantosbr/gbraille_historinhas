/**
 * Copyright (C) 2014 Antonio Rodrigo
 * 
 * This file is part of GBraille Project.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; If not, see <http://www.gnu.org/licenses/>.
 */

package com.gbraille.historinhas;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gbraille.libraries.LogMessages;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import de.akquinet.android.androlog.Log;
import android.content.Context;

public class CloudMessagingFunctions {
	/* google cloud services sender ID - this value cames from assets/gcm.xml file */
	String SENDER_ID;
	
	/* google cloud services server URL - this value cames from assets/gcm.xml file*/
	String SERVER_URL;
	
	private final String PROPERTY_REG_ID = "registration_id";
 	private final String PROPERTY_APP_VERSION = "appVersion";
 	private final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
 	
 	private String gcmFile = "gcm.xml";
 	
 	Context context;
 	
 	private final String TAG = "CloudMessagingFunctions";
 	
 	public String getPropertyRegId(){
 		return PROPERTY_REG_ID;
 	}
 	public String getPropertyAppVersion(){
 		return PROPERTY_APP_VERSION;
 	}
 	public int getPlayServicesResolutionRequest(){
 		return PLAY_SERVICES_RESOLUTION_REQUEST;
 	}
	
	public String getSenderId(){
		return SENDER_ID;
	}
	public void setSenderId(String id){
		SENDER_ID = id;
	}
	
	public String getServerUrl(){
		return SERVER_URL;
	}
	public void setServerUrl(String url){
		SERVER_URL = url;
	}
	
	public CloudMessagingFunctions(Context context){
		this.context = context;
		readGoogleCloudMessagingXMLFile(context);
	}
	
	public boolean readGoogleCloudMessagingXMLFile(Context context) {
		try {
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();    
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder(); 
		    Document doc = dBuilder.parse(context.getAssets().open(gcmFile));
		    doc.getDocumentElement().normalize();

		    NodeList nodeContatos = doc.getElementsByTagName("entrada");

		    Node item = nodeContatos.item(0);

		    if (item.getNodeType() == Node.ELEMENT_NODE) {
		        Element element = (Element) item;
		        Node nodeSenderId = element.getElementsByTagName("senderid").item(0).getChildNodes().item(0);
		        Node nodeServerUrl = element.getElementsByTagName("serverurl").item(0).getChildNodes().item(0);
		        setSenderId(nodeSenderId.getNodeValue().toString());
		        setServerUrl(nodeServerUrl.getNodeValue().toString());
		        Log.i(TAG, LogMessages.MSG_CLOUD_MESSAGING_KEY_READ);
		        return true;
		    }
		    Log.i(TAG, LogMessages.MSG_ERROR_DURING_FILE_READING + gcmFile);
	        return true;
		}
		catch(Exception e){
			Log.i(TAG,"Exception: " + e);
		}
		return false;
	}	
}
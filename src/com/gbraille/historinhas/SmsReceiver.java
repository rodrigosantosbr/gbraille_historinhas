/**
 * Copyright (C) 2014 Antonio Rodrigo
 * 
 * This file is part of GBraille Project.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; If not, see <http://www.gnu.org/licenses/>.
 */

package com.gbraille.historinhas;

import com.gbraille.libraries.LogFunctions;
import com.gbraille.libraries.LogMessages;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.telephony.SmsMessage;
import de.akquinet.android.androlog.Log;

/* intercept the SMS and retrieve the body & the sender */
public class SmsReceiver extends BroadcastReceiver {
	
	/* class name */
	private final String TAG = "SmsReceiver";
	
	/* database functions */
	DatabaseFunctions databaseFunctions = new DatabaseFunctions();
	
	/* log functions */
    LogFunctions logFunctions = new LogFunctions(true,"hist");
		
	/**
	 * Receives broadcast notifications of all telephone SMS events and sends then to a database
	 * @see http://developer.android.com/reference/android/appwidget/AppWidgetProvider.html#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
    public void onReceive(Context context, Intent intent) {
		Bundle extras = intent.getExtras();
        if (extras == null) {
            return;
        }
        
        Object[] pdus = (Object[]) extras.get("pdus");
        for (int i = 0; i < pdus.length; i++) {
        	
        	Log.i(TAG,LogMessages.MSG_SMS_MESSAGE_RECEIVED);
        	
        	// intercepted message
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
            String sender = smsMessage.getOriginatingAddress();
            String body = smsMessage.getMessageBody().toString();     
            
            SQLiteDatabase myDB = null;
            
            try {
            	myDB = context.openOrCreateDatabase(databaseFunctions.getNomeBD(), 0, null);    			
    			myDB.beginTransaction();
    			String table = "mensagensrecebidas";
    			
    			ContentValues cv = new ContentValues();
        		cv.put("telefoneorigem", sender);
        		cv.put("texto", body);
        		    
        		if (databaseFunctions.isTableExists(myDB, table)){
        			myDB.insert(table, null, cv);        	
        			myDB.setTransactionSuccessful(); 
        			myDB.endTransaction(); 
        			myDB.close();
        			Log.i(TAG, LogMessages.MSG_BD_RECEIVED_MESSAGE_SAVED);
        		}
        		else{
        			Log.i(TAG,LogMessages.MSG_BD_ERROR_TABLE_MENSAGENS_RECEBIDAS_DOES_NOT_EXISTS);
		    		logFunctions.logTextFile(LogMessages.MSG_BD_ERROR_TABLE_MENSAGENS_RECEBIDAS_DOES_NOT_EXISTS);
        		}
            }
            catch(Exception e) {
    			Log.e(TAG, "Erro: ", e);
    		}
            
            Intent it = new Intent("SmsMessage.intent.MAIN");
            it.putExtra("msg", sender+":"+body);
            
            context.sendBroadcast(it);            
        }
	}
}
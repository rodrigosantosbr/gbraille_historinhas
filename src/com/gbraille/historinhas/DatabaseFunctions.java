/**
 * Copyright (C) 2014 Antonio Rodrigo
 * 
 * This file is part of GBraille Project.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; If not, see <http://www.gnu.org/licenses/>.
 */

package com.gbraille.historinhas;

import de.akquinet.android.androlog.Log;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseFunctions {
	/* SQLite database name */
	private final String NOMEBD = "gbraillehistorinhas";
	
	/**
	 * retrieves the database name
	 *
	 * @author      Antonio Rodrigo
	 * @version     1.0 
	 * @return      database name
	 */	
	public String getNomeBD(){
		return NOMEBD;
	}
	
	/**
	 * check if a table exist in a database
	 *
	 * @author      Antonio Rodrigo
	 * @version     1.0 
	 * @param       database name
	 * @param       table name
	 * @return      true or false
	 */	
	public boolean isTableExists(SQLiteDatabase db, String tableName){
	    if (tableName == null || db == null || !db.isOpen()){
	        return false;
	    }
	    Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[] {"table", tableName});
	    if (!cursor.moveToFirst()){
	        return false;
	    }
	    int count = cursor.getInt(0);
	    cursor.close();
	    return count > 0;
	}
	
	public void dropDatabase(Context ctx) {
		SQLiteDatabase myDB= null;
		try {
			myDB = ctx.openOrCreateDatabase(getNomeBD(), Context.MODE_PRIVATE, null);
			myDB.beginTransaction();
			myDB.execSQL("DROP TABLE IF EXISTS configuracoes");
			myDB.execSQL("DROP TABLE IF EXISTS mensagem");
			myDB.execSQL("DROP TABLE IF EXISTS mensagensrecebidas");
			myDB.setTransactionSuccessful(); 
    		myDB.endTransaction(); 
    		myDB.close();
		}
		catch(Exception e) {
			Log.e("Error", "Error", e);
		}        
    }
	
	public boolean createDatabase(Context ctx){
		SQLiteDatabase myDB= null;
		
		try {
			myDB = ctx.openOrCreateDatabase(getNomeBD(), Context.MODE_PRIVATE, null);
			
			myDB.beginTransaction();
			
			myDB.execSQL("CREATE TABLE IF NOT EXISTS configuracoes ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, configuracao VARCHAR(255) NOT NULL, valor VARCHAR(255) NOT NULL );");	
			myDB.execSQL("CREATE TABLE IF NOT EXISTS mensagem ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, telefonedestino VARCHAR(20) NOT NULL, texto VARCHAR(255) NOT NULL );");
			myDB.execSQL("CREATE TABLE IF NOT EXISTS mensagensrecebidas ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, telefoneorigem VARCHAR(20) NOT NULL, texto VARCHAR(255) NOT NULL );");
    	
			// insert the id = 1 in the table mensagem			
    		ContentValues cv = new ContentValues();
    		cv.put("telefonedestino", "");
    		cv.put("texto", "");    		    	
    		myDB.insert("mensagem", null, cv);
    		
    		// insert the id = 1 and configuracao = 'telefoneprofessor' in the table configuracoes			
    		cv = new ContentValues();
    		cv.put("configuracao", "");
    		cv.put("valor", "");    		    	
    		myDB.insert("configuracoes", null, cv);
    	
    		myDB.setTransactionSuccessful(); 
    		myDB.endTransaction(); 
    		myDB.close();
    		
    		return true;
		}
		catch(Exception e) {
			Log.e("Error", "Error", e);
		}
		return false;
	}
}
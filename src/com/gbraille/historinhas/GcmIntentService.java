/**
 * Copyright (C) 2014 Antonio Rodrigo
 * 
 * This file is part of GBraille Project.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; If not, see <http://www.gnu.org/licenses/>.
 */

package com.gbraille.historinhas;

import com.gbraille.libraries.LogFunctions;
import com.gbraille.libraries.LogMessages;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import de.akquinet.android.androlog.Log;
 
/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    
    private String mensagem, tipoMensagem;
    
    private DatabaseFunctions databaseFunctions = new DatabaseFunctions();
    private LogFunctions logFunctions = new LogFunctions(true,"hist");
    
 
    public GcmIntentService() {
        super("GcmIntentService");
    }
    
    public static final String TAG = "GcmIntentService";
 
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        mensagem = extras.getString("dataKey");
        
        // informa��es sobre o tipo da mensagem entregue (se � um telefone, etc)
        tipoMensagem = extras.getString("tipoMensagem");
        Log.i(TAG, LogMessages.MSG_TYPE_MESSAGE + tipoMensagem);
        
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
 
        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } 
            else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " + extras.toString());            
            }
            // If it's a regular GCM message, do some work.
            else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                for (int i = 0; i < 5; i++) {
                    Log.i(TAG, LogMessages.MSG_WORKING + (i + 1) + "/5 @ " + SystemClock.elapsedRealtime());
                    try {
                        Thread.sleep(5000);
                    } 
                    catch (InterruptedException e) {
                    }
                }
                Log.i(TAG, LogMessages.MSG_COMPLETED_WORK_AT + SystemClock.elapsedRealtime());
                // Post notification of received message.
                sendNotification(LogMessages.MSG_RECEIVED + mensagem);
                Log.i(TAG, LogMessages.MSG_RECEIVED + mensagem);
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }
 
    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
 
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, ConfiguracoesActivity.class), 0);
 
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.ic_launcher)
        .setContentTitle("GCM NOTIFICATION")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(msg))
        .setContentText(msg);
 
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        
        if ( tipoMensagem.equals("telefoneProfessora") ){        	
        	try {
        		SQLiteDatabase db = this.openOrCreateDatabase(databaseFunctions.getNomeBD(), MODE_PRIVATE, null);
        		db.beginTransaction();
        		String table = "configuracoes";
        	
        		ContentValues cv = new ContentValues();
        		cv.put("configuracao", "telefoneprofessor");
        		cv.put("valor", mensagem);
        		
        		if (databaseFunctions.isTableExists(db, table)){
        	   		db.insert(table, null, cv);
        	   		db.setTransactionSuccessful(); 
        	   		db.endTransaction(); 
        	   		db.close();
        	   		Log.i(TAG, LogMessages.MSG_BD_TEACHER_PHONE_NUMBER_SAVED);
        		}
        		else{
        			Log.i(TAG,LogMessages.MSG_BD_ERROR_TABLE_CONFIGURACOES_DOES_NOT_EXISTS);
		    		logFunctions.logTextFile(LogMessages.MSG_BD_ERROR_TABLE_CONFIGURACOES_DOES_NOT_EXISTS);
        		}
        		
        		try {
                    Thread.sleep(1000);
                } 
                catch (InterruptedException e) {
                }
        		
        		//MainFunctions.speakWords("O TELEFONE DA PROFESSORA FOI ATUALIZADO");
        	}
    		catch(Exception e) {
    			Log.e("Error", "Error", e);
    		}	
        }
    }
}
/**
 * Copyright (C) 2014 Antonio Rodrigo
 * 
 * This file is part of GBraille Project.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; If not, see <http://www.gnu.org/licenses/>.
 */

package com.gbraille.historinhas;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.gbraille.libraries.LogMessages;
import de.akquinet.android.androlog.Log;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.UtteranceProgressListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class MainScreenActivity extends AccessibleAbstractActivityBrailleTemplate {
	
	/* cloud messaging functions */
	private CloudMessagingFunctions cloudMessagingFunctions;
	
	/**
	 * onclick actions for buttons
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0 
	 */
	protected void setOnClickForButtons() {		
		/* write text - onclick event */
		this.buttons[0].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				stopServices();
				logFunctions.logTextFile(LogMessages.MSG_BUTTON_WAS_ACCESSED + (String) buttons[0].getTag());
				Intent i = new Intent(getActivity(), MenuEscreverActivity.class);
				startActivity(i);
				finish();
			}
		});

		/* message box - onclick event */
		this.buttons[1].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				stopServices();
				logFunctions.logTextFile(LogMessages.MSG_BUTTON_WAS_ACCESSED + (String) buttons[1].getTag());
				Intent i = new Intent(getActivity(), CaixaEntradaActivity.class);
				startActivity(i);
				finish();
			}
		});

		/* options - onclick event */
		this.buttons[2].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				stopServices();
				logFunctions.logTextFile(LogMessages.MSG_BUTTON_WAS_ACCESSED + (String) buttons[2].getTag());
				Intent i = new Intent(getActivity(), ConfiguracoesActivity.class);
				startActivity(i);
				finish();
			}
		});

		/* exit - onclick event */
		this.buttons[3].setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				destroyServices();
				logFunctions.logTextFile(LogMessages.MSG_BUTTON_WAS_ACCESSED + (String) buttons[3].getTag());
				System.exit(0);
				moveTaskToBack(true);
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * Called when the activity is first created.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		screenName = "Historinhas: Menu Principal";		
		cloudMessagingFunctions = new CloudMessagingFunctions(this);	
		super.onCreate(savedInstanceState);
		
		nodeDescriptionString = new String[totalButtons];
		
		fillScreenOptionsFromXMLFile("mainactivityoptions.xml","option");
	}

	/**
	 * retrieves the Layout resource
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 * @return layout resource id
	 */
	@Override
	protected int getLayoutResourceId() {
		return R.layout.activity_main;
	}

	/**
	 * retrieves the current activity
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 * @return current activity
	 */
	@Override
	protected Activity getActivity() {
		return MainScreenActivity.this;
	}

	/**
	 * adds the buttons to variables
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 */
	@Override
	protected void setViews() {
		buttons = new ImageButton[totalButtons];
		buttons[0] = (ImageButton) findViewById(R.id.botao1);
		buttons[1] = (ImageButton) findViewById(R.id.botao2);
		buttons[2] = (ImageButton) findViewById(R.id.botao3);
		buttons[3] = (ImageButton) findViewById(R.id.botao4);	
	}

	/**
	 * set the number of buttons in the current activity
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0 
	 */
	@Override
	protected void setNumberButtons() {
		this.totalButtons = 4;		
	}
	
	/**
	 * retrieves the Activity's Linear Layout
	 * 
	 * @author Antonio Rodrigo
	 * @version 1.0
	 * @return LinearLayout
	 */
	@Override
	protected LinearLayout getLayout() {
		return (LinearLayout) this.findViewById(R.id.layoutMainActivity);
	}
	
	protected void setTts() { 
        if( Build.VERSION.SDK_INT  >= 15 ){
        	final String thisTAG = this.TAG;
            myTTS.setOnUtteranceProgressListener(new UtteranceProgressListener() {            	
                @Override
                public void onDone(String utteranceId){
                	Log.i(thisTAG, "Text to speech finished");
                	if (utteranceId.equals("EXIT_APPLICATION")){
			        	hapticFunctions.vibrateOnExit();
			        	destroyServices();
			        	finish();
					}
                	else if (utteranceId.equals("STARTING_APPLICATION")){
            			MainScreenActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            	// vibrates and ebables keyboard when application starts
                            	hapticFunctions.vibrate(1);                    			                    			
                            }
                        });      			
                	}
                }
 
                @Override
                public void onError(String utteranceId){
                	Log.i(thisTAG, "Error in processing Text to speech");
                }
 
                @Override
                public void onStart(String utteranceId){
                	Log.i(thisTAG, "Started speaking");
                }
            });
        }
    }
	
	/**
	 * fillScreenOptionsFromXMLFile
	 *     Loads the contents of the file named 'applications.xml' and get its params
	 * @author Antonio Rodrigo
	 * @version 1.0
	 */
	private void fillScreenOptionsFromXMLFile(String fileName, String nodeScreenOptionsTag) {
		try {
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();    
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder(); 
		    Document doc = dBuilder.parse(getAssets().open(fileName));
		    doc.getDocumentElement().normalize();

		    NodeList nodeScreenOptions = doc.getElementsByTagName(nodeScreenOptionsTag);
		    
		    for (int i = 0; i < nodeScreenOptions.getLength(); i++) {
		    	final int j = i;
		    	Node item = nodeScreenOptions.item(i);

		    	if (item.getNodeType() == Node.ELEMENT_NODE) {
		    		Element element = (Element) item;
		    		Node nodeDescricao = element.getElementsByTagName("description").item(0).getChildNodes().item(0);
		    		Node nodeIcone = element.getElementsByTagName("icon").item(0).getChildNodes().item(0);
		    		
		    		Log.i(TAG, nodeDescricao.getNodeValue());
		    		
		    		final String desc = nodeDescricao.getNodeValue();
		    		
		    		nodeDescriptionString[i] = nodeDescricao.getNodeValue();
		    		
		    		int imageResource = getResources().getIdentifier(nodeIcone.getNodeValue(), null, getPackageName());
		    		Drawable res = getResources().getDrawable(imageResource);
		    				    
		    		buttons[i].setTag(nodeDescricao.getNodeValue());
		    		buttons[i].setImageDrawable(res);
		    		buttons[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
						public void onFocusChange(View v, boolean hasFocus) {
							if (hasFocus) {
								Log.i(TAG, "Button " + desc + " was selected");
								logFunctions.logTextFile("Button " + desc + " was selected");
								if (j == 0 || j == 1 || j == 2){
									speakButton(desc, 0, 1.0f);
								}
								else{
									speakButton(desc, 1.0f, 0);
								}
							}
						}
					});
		    		
		    	}
		    }
		}
		catch(Exception e){
		     e.printStackTrace();
		}
	}
}